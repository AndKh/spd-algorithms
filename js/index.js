/*
 Задан массив целых чисел. Определить максимальную длину (+ позиции
 начала и конца) последовательности идущих подряд различных элементов.
*/

function createArrIntegers(arrLength) {
    var arr = [];
    var generateInteger = function (maxNum, minNum) {
        return Math.round(Math.random() * (maxNum - minNum + 1) + minNum);
    };

    for (var i = 0; i < arrLength; i++) {
        arr[i] = generateInteger(0, 10);
    }
    return arr;
}

var arrInt = createArrIntegers(20);
console.info(arrInt);

function drawArrSequences(arr) {
    var arrMain = arr.map(createArrSequences).sort(sortArrSequence);

    function createArrSequences(currentValue, index, array) {
        var arrResult = [currentValue];

        for (var i = index + 1; i < array.length; i++) {
            for (var j = 0; j < arrResult.length; j++) {
                if (array[i] === arrResult[j]) {
                    return [arrResult, {'start': index, 'end': index + arrResult.length - 1}];
                }
            }
            arrResult.push(array[i]);
        }

        return [arrResult, {'start': index, 'end': index + arrResult.length - 1}];
    }

    function sortArrSequence(a, b) {
        return b[0].length - a[0].length;
    }

    return arrMain.filter(function (val) {
        if (val[0].length === arrMain[0][0].length) {
            return val[0];
        }
    });
}

console.log("Best length of Array(s) - " + drawArrSequences(arrInt));

/*
 Есть строка, состоящая из слов. Необходимо реализовать наиболее
 эффективный алгоритм перестановки слов в обратном порядке в предложении(без выделения дополнительной памяти).
 */
var someString = "The London is a capital of Great Britain";
console.info("Reverse for string - " + someString.split(" ").reverse().join(" "));

/*
 Разложить книги по 2­м стопкам так, чтобы разница высот была минимальной.
 */

var books = [{ book: "A", pages: 300 }, { book: "B", pages: 100 }, { book: "C", pages: 175}, { book: "D", pages: 50 }, { book: "E", pages: 345}];

function sortBooks (arr) {
    console.info(arr);
    var sortBasedArr = arr.sort(function (a, b) {
        return b.pages - a.pages;
    });
    var sumPages = arr.reduce(function (previousValue, currentValue) {
            return previousValue += currentValue.pages;
    }, 0);

    var halfHeight = sumPages/2;

    var stacks = [
        {
            name: "stack1",
            pages: 0,
            books: []
        },
        {
            name: "stack2",
            pages: 0,
            books: []
        }
    ];

    for (var i = 0; i < arr.length; i++ ) {
        if ((stacks[0].pages + sortBasedArr[i].pages) < halfHeight) {
            stacks[0].pages += sortBasedArr[i].pages;
            stacks[0].books.push(sortBasedArr[i]);
        } else {
            stacks[1].pages += sortBasedArr[i].pages;
            stacks[1].books.push(sortBasedArr[i]);
        }
    }

    return stacks;
}


console.log("Two stacks of books - " + sortBooks(books));